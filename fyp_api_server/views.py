import base64
import json

from django.core.exceptions import BadRequest
from django.http import HttpResponse

from fyp_api_server.designer import auth, persist
from fyp_api_server.designer.test import get_return_structure
from fyp_api_server.graph.graph import Graph
from fyp_api_server.models import Config


def handle_start(request, requested_config):
    #takes config id
    #returns array of name, description, question, image in b64 encoding
    try:
        found_config = Config.objects.filter(config_id=requested_config).values('name','description','question','image')
        found_config = list(found_config)
        found_config = found_config[0]#There will only ever be one config so this gets the config object
        if found_config['image']== '':#for user testing purposes to prevent everyone needing to upload images
            found_config['image']=list(Config.objects.filter(config_id='u3VQIpge').values('image'))[0]['image']

        img_encoding = base64.b64encode(open(found_config['image'], "rb").read()).decode('utf-8')
        #The header of the b64 string isn't encoded correctly so reforming it
        split = img_encoding.split('base64',1)
        split2 = split[0].split('/',1)
        found_config['image'] = 'data:image/'+ split2[1] + ';base64,' + split[1]
        found_config['description'] = found_config['description'].replace('\r','')

        return HttpResponse(json.dumps(found_config),
                            content_type='application/json; charset=utf8')
    except:
        raise BadRequest('Invalid request')



def handle_graph(request, requested_config, query_string):#gets the nodes and types for this config
    try:
        graph_instance = Graph(requested_config, query_string)
        types=graph_instance.types_list
        nodes=graph_instance.nodes
        returned_response = {'types' : types,'nodes' : nodes}

        return HttpResponse(json.dumps(returned_response),
                            content_type='application/json; charset=utf8')
    except:
        raise BadRequest('Invalid request')


def handle_new(request):#generates a new config id and auth code and creates config in db
    return HttpResponse(json.dumps(auth.create_new()),
                        content_type='application/json; charset=utf8')



def handle_load(request, requested_config, access_code):#loads existing config to designer
    if not auth.check(requested_config,access_code):
        raise BadRequest('Either config or access code incorrect')

    returned_response = persist.load_from_db(requested_config)

    return HttpResponse(json.dumps(returned_response),
                        content_type='application/json; charset=utf8')



def handle_save(request, requested_config, access_code):#saves config from designer
    if not auth.check(requested_config,access_code):
        raise BadRequest('Either config or access code incorrect')

    try:
        json_response = json.loads(request.body)
        persist.save_to_db(json_response)
        return HttpResponse(status=200)
    except:
        raise BadRequest('Submitted data invalid')

def handle_test(request, requested_config, access_code):#generates dot structure json for any return from specified api
    if not auth.check(requested_config,access_code):
        raise BadRequest('Either config or access code incorrect')

    try:
        json_response = json.loads(request.body)
        returned_response = get_return_structure(json_response['insertions'],json_response['url'])
        return HttpResponse(json.dumps(returned_response),
                            content_type='application/json; charset=utf8')
    except:
        raise BadRequest('Submitted data invalid')

