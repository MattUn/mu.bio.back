from django.contrib import admin

# Register your models here.
from fyp_api_server.models import *

admin.site.register(Config)
admin.site.register(Type)
admin.site.register(Variable)
admin.site.register(InsertionPoint)
admin.site.register(TypeLink)
admin.site.register(LinkOption)
admin.site.register(LinkConn)
