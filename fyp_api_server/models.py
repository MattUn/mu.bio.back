from django.db import models

class Config(models.Model):
    config_id = models.CharField('Config ID', max_length=8,unique=True, blank=False, null=False)
    config_password = models.CharField('Config password ', max_length=256, blank=False, null=False)
    name = models.TextField('Name')
    description = models.TextField('Description')
    question = models.TextField('Question')
    image = models.ImageField(upload_to ='Imguploads/')

    def __str__(self):
        return str(self.config_id)+' - '+str(self.name)



class Type(models.Model):
    config = models.ForeignKey(Config, blank=False, null=False, on_delete=models.CASCADE)
    shape = models.CharField('Display shape', max_length=128)
    colour = models.CharField('Hex colour', max_length=7)
    name = models.TextField('Name')
    url = models.TextField('URL')
    attribution = models.TextField('Source attribution')
    start = models.BooleanField('Is start type')

    def __str__(self):
        return str(self.config)+' - '+str(self.name)


class Variable(models.Model):
    type = models.ForeignKey(Type, blank=False, null=False, on_delete=models.CASCADE)
    ret_name = models.TextField('Returned name')
    display_name = models.TextField('Display name', blank=True, null=True)#If null then don't show

    def __str__(self):
        return str(self.type)+' - '+str(self.display_name)


class InsertionPoint(models.Model):
    type = models.ForeignKey(Type, blank=False, null=False, on_delete=models.CASCADE)
    name = models.TextField('Name in URL')

    def __str__(self):
        return str(self.type) + ' - ' + str(self.name)


class TypeLink(models.Model):
    start = models.ForeignKey(Type, blank=False, null=False, on_delete=models.CASCADE, related_name='start_type')
    end = models.ForeignKey(Type, blank=False, null=False, on_delete=models.CASCADE, related_name='end_type')

    def __str__(self):
        return str(self.start) + ' to ' + str(self.end)


class LinkConn(models.Model):
    type_link = models.ForeignKey(TypeLink, blank=False, null=False, on_delete=models.CASCADE)
    variable = models.ForeignKey(Variable, blank=False, null=False, on_delete=models.CASCADE)
    insertion_point = models.ForeignKey(InsertionPoint, blank=False, null=False, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.type_link) + ' - ' + str(self.variable) + ' inserting to ' + str(self.insertion_point)


class LinkOption(models.Model):
    type_link = models.ForeignKey(TypeLink, blank=False, null=False, on_delete=models.CASCADE)
    option = models.CharField('Enabled link/cleansing option', max_length=64)

    def __str__(self):
        return str(self.type_link) + ' - ' + str(self.option)
