import json
from collections import defaultdict

import requests
import re as regex


from fyp_api_server.models import Variable, Config, Type, InsertionPoint, LinkConn, TypeLink


class Graph:
    def __init__(self, requested_config, query_string):
        self.next_node_id = 0
        self.config = Config.objects.filter(config_id=requested_config).first()
        self.query_string = query_string
        self.types = self.load_types()
        self.types_list = self.load_types_list()
        self.nodes = []
        self.make_nodes()

    def load_types(self):
        types = Type.objects.filter(config=self.config)
        if not types:
            raise Exception("no types found for config")
        return types

    def load_types_list(self):#useful representation of types
        types = self.types.values('shape', 'colour', 'name')
        return list(types)

    def make_nodes(self):#starting the new node process with the start type
        start_type = self.types.filter(start=True).first()
        self.new_node(start_type)

    def new_node(self, node_type, existing_vars = None, source_node_id = 0):
        try:
            if existing_vars is None:#Used on the first node to add start query in
                existing_vars = {'BIO.start_query': self.query_string}

            #building url
            url = self.new_node_url(existing_vars, node_type)

            out = self.new_node_arrays(node_type, url)

            for lst in out:  # one iteration for each node
                lst = out[lst]
                merged_dicts = {}
                for dct2 in lst:
                    merged_dicts.update(dct2)
                out_vars = existing_vars | merged_dicts
                # add node
                current_node_id = self.next_node_id
                self.next_node_id = self.next_node_id + 1
                node = {'type': node_type.name, 'node_id_source': source_node_id, 'node_id': current_node_id,
                        'attribution': node_type.attribution, 'title': build_title(out_vars, node_type),
                        'description': build_description(out_vars, node_type)}
                self.nodes.append(node)

                # go to next nodes
                for end_id in TypeLink.objects.filter(start=node_type).values('end_id', 'id'):
                    end_type = Type.objects.get(id=end_id['end_id'])
                    self.new_node(end_type, out_vars, current_node_id)
        except:
            print("couldn't make a node")

    def new_node_url(self, existing_vars, node_type):
        url = node_type.url
        for insertion_point in InsertionPoint.objects.filter(type=node_type):#finding matching var and replacing insert point with it
            if self.next_node_id >= 1:
                to_insert = LinkConn.objects.filter(insertion_point=insertion_point).values('variable__ret_name')[0][
                    'variable__ret_name']
            else:  # first node only
                to_insert = 'BIO.start_query'
            to_insert = existing_vars[to_insert]
            url = url.replace(insertion_point.name, to_insert)
        return url

    def new_node_arrays(self, node_type, url):#handle arrays in json. treats non-arrays as 1 length array
        new_vars = get_values(url, node_type, full_lists=True)
        dct = defaultdict(list)
        non_array_list = []#includes everything which isn't in an array
        for new_var in new_vars:
            search_index = regex.search("\.(\d+)\.", new_var)#finds any numbers indicating arrays
            new_var_rename = regex.sub(r"\.(\d+)\.", r'_LIST.', new_var)#stores it with num replaced with list str format
            if search_index:#add the var to the correct index for outbound node
                tmp = search_index.group(1)
                dct[tmp].append({new_var_rename: new_vars[new_var]})
            else:#anything not an array item add to non array to add on later
                non_array_list.append({new_var: new_vars[new_var]})
                # add to backup list. This list will then be EXTENDed onto each list at the end.
        out = defaultdict(list)
        if not dct:#if no arrays found then just output the non-array parts
            out[0] = non_array_list
        else:
            for dct_list_key in dct:#otherwise add relevant list items to each key and all the non-list items
                out[dct_list_key].extend(dct[dct_list_key])
                out[dct_list_key].extend(non_array_list)
        return out


def build_title(new_vars, node_type):#create title for use on node display
    ret_name = Variable.objects.filter(type=node_type, display_name='TITLE')
    if not ret_name:
        return 'No title found'
    ret_name = ret_name.values('ret_name')
    ret_name = ret_name.first()
    ret_name = ret_name['ret_name']
    return  new_vars[ret_name]

def build_description(new_vars, node_type):#build description from all requested vars
    built_str = ''
    display_variables = Variable.objects.filter(type=node_type).values('ret_name','display_name')
    for dis_var in display_variables:
        if dis_var['display_name'] == 'TITLE':
            continue
        for new_var in new_vars:
            if dis_var['ret_name'] == new_var:
                built_str+=dis_var['display_name']+':\n'
                built_str+= new_vars[new_var] + '\n\n'
    return  built_str

def get_values(url, node_type='', only_selected = True, full_lists=False):
    response = requests.get(url).text
    response = regex.sub(r"{('|\")\d+('|\"):", r'{"BIOgeneric":', response)#replace any keys with variable values
    json_response = json.loads(response)
    list_elements = []
    old = ''
    if not node_type == '':
        old=node_type.name
    get_json_dot(json_response, list_elements, old='', full_lists=full_lists)

    list_elements_cut = []
    included_vars = list(Variable.objects.values('ret_name'))
    included_vars = [dict_ret['ret_name'] for dict_ret in included_vars]
    for element in list_elements:
        if len(regex.findall('_LIST', element)) < 3:#always remove nested lists - some exceptions
            if only_selected:
                if regex.sub(r"\.(\d+)\.", r'_LIST.', element.split(' : ',1)[0]) in included_vars:
                    list_elements_cut.append(element)
            else:
                list_elements_cut.append(element)
    return dict(item.split(' : ') for item in list_elements_cut)

#From https://techtldr.com/convert-json-to-dot-notation-with-python/
#with modifications to allow returning of a list and grouping json lists
def get_json_dot(val, list_elements, old='', full_lists=True):
    if isinstance(val, dict):
        if val:#if not empty dict
          for k in val.keys():#recur within the dict
            get_json_dot(val[k], list_elements, old + '.' + str(k), full_lists)
        else:
         list_elements.append('{} : {}'.format(old,'{}'))
    elif isinstance(val, list):
        if val:
            if full_lists:#false to ignore more than the first element of the list
                for i, k in enumerate(val):#recur for each element in list
                    get_json_dot(k, list_elements, old + '.' + str(i), full_lists)
            else:
                get_json_dot(val[0], list_elements, old + '_LIST', full_lists)

        else:
          list_elements.append('{} : []'.format(old,'{}'))
    else:
        list_elements.append('{} : {}'.format(old,str(val)))