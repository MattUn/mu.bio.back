from django.apps import AppConfig


class FypApiServerConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'fyp_api_server'
