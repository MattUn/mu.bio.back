from fyp_api_server.graph.graph import get_values


def get_return_structure(insertion_points, url):
    # takes list of insertion points with values + url
    # returns dot notation format
    for point in insertion_points:
        url = url.replace(point,insertion_points[point])
    result = get_values(url,only_selected=False);
    return result