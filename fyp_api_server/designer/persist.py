import base64
from collections import defaultdict

from django.db.models import Q

from fyp_api_server.models import Config, Type, InsertionPoint, Variable, TypeLink, LinkConn


def save_to_db(json_response):#core method for saving full state sent from designer to db
    requested_config = json_response['config_code']

    config = Config.objects.filter(config_id=requested_config).first()
    config.name = json_response['summary_name']
    config.description = json_response['summary_description']
    if len(json_response['summary_image'])>0:#don't overwrite summary image with blank
        with open("Imguploads/"+requested_config+".image", "wb") as fh:
            fh.write(base64.urlsafe_b64decode(json_response['summary_image']))#write to file and store ref in db
        config.image = "Imguploads/"+requested_config+".image"
    config.question = json_response['start_prompt']
    config.save()
    save_type(json_response,config)
    save_conn(json_response,config)


def save_type(json_response,config):
    json_type = json_response['type']
    Type.objects.filter(config=config).delete()
    for type_shape in json_type:#per type
        type_indiv = json_type[type_shape]
        if len(type_indiv['name'])>0:
            new_db_type = Type(config=config, shape=shape_to_string(type_shape), colour=colour_lookup(type_shape),\
                               name=type_indiv['name'], url=type_indiv['url'], attribution=type_indiv['attribution'],\
                               start=type_indiv['is_start'])
            new_db_type.save()

            #insertions
            for insertion_point in type_indiv['insertions']:
                new_db_insertion = InsertionPoint(type=new_db_type, name=insertion_point)
                new_db_insertion.save()

            #retvals
            for ret_var in type_indiv['returns']:
                value = type_indiv['returns'][ret_var]
                if len(value)>0:
                    new_db_ret_var = Variable(type=new_db_type, ret_name=ret_var, display_name=value)
                    new_db_ret_var.save()

def save_conn(json_response, config):
    json_conn = json_response['conn']
    for end_type_shape in json_conn:#per end type of connection (this always is first)
        start_type_shape = json_conn[end_type_shape]#per start type in each end type
        #TypeLink
        start_type_type = Type.objects.filter(config=config, shape=shape_to_string(next(iter(start_type_shape)))).first()
        end_type_type = Type.objects.filter(config=config, shape=shape_to_string(end_type_shape)).first()
        new_db_typelink = TypeLink(start=start_type_type, end=end_type_type)
        new_db_typelink.save()

        for insert_point in start_type_shape[next(iter(start_type_shape))]:
            # LinkConn
            variable = start_type_shape[next(iter(start_type_shape))][insert_point]
            var_variable = Variable.objects.filter(type__config=config, display_name=variable).first()
            ip_insertion_point = InsertionPoint.objects.filter(type__config=config, name=insert_point).first()
            new_db_linkconn = LinkConn(type_link=new_db_typelink, variable=var_variable, insertion_point=ip_insertion_point)
            new_db_linkconn.save()



def load_from_db(requested_config):#load values for designer. Image not sent
    config = Config.objects.filter(config_id=requested_config)
    rtn_dct = {}

    val_config=config.values('name','description','question')[0]
    rtn_dct['summary_name'] = val_config['name']
    rtn_dct['summary_description'] = val_config['description']
    rtn_dct['start_prompt'] = val_config['question']

    rtn_dct['type'] = load_type(config.first())
    rtn_dct['conn'] = load_conn(config.first())
    return rtn_dct

def load_type(config):
    rtn_dct = {}
    types = Type.objects.filter(config=config)
    for qs_type in types:
        type_dct = {}
        type_dct['name'] = qs_type.name
        type_dct['url'] = qs_type.url
        type_dct['attribution'] = qs_type.attribution
        type_dct['is_start'] = qs_type.start

        #insertions
        insertion_dct = {}
        insertion_points = InsertionPoint.objects.filter(type=qs_type)
        for qs_ip in insertion_points:
            insertion_dct[qs_ip.name] = ''
        type_dct['insertions'] = insertion_dct

        #retvals
        retval_dct = {}
        variables = Variable.objects.filter(type=qs_type)
        for qs_var in variables:
            retval_dct[qs_var.ret_name] = qs_var.display_name
        type_dct['returns'] = retval_dct

        rtn_dct[string_to_shape(qs_type.shape)] = type_dct
    return rtn_dct

def load_conn(config):
    links = TypeLink.objects.filter(Q(start__config=config) | Q(end__config=config))#OR to pick up all links for config
    conn_dct = defaultdict(dict)
    for qs_link in links:
        start_type = qs_link.start
        end_type = qs_link.end
        link_conns = LinkConn.objects.filter(type_link=qs_link)
        for qs_link_conn in link_conns:
            existing = {}
            if string_to_shape(end_type.shape) in conn_dct and \
                string_to_shape(start_type.shape) in conn_dct[string_to_shape(end_type.shape)]:#adding to existing if exists
                existing = conn_dct[string_to_shape(end_type.shape)][string_to_shape(start_type.shape)]
            conn_dct[string_to_shape(end_type.shape)][string_to_shape(start_type.shape)] = \
                {qs_link_conn.insertion_point.name:qs_link_conn.variable.display_name} | existing#otherwise creating new

    return conn_dct

def shape_to_string(shape_char):#for save
    match shape_char:
        case '■':
            return 'square'
        case '●':
            return 'dot'
        case '◆':
            return 'diamond'
        case '▲':
            return 'triangle'
        case '▼':
            return 'triangleDown'
        case _:
            return ''

def string_to_shape(shape_str):#for load
    match shape_str:
        case 'square':
            return '■'
        case 'dot':
            return '●'
        case 'diamond':
            return '◆'
        case 'triangle':
            return '▲'
        case 'triangleDown':
            return '▼'
        case _:
            return ''

def colour_lookup(shape_char):  #for load
    match shape_char:
        case '■':
            return '#d57127'
        case '●':
            return '#cc79a8'
        case '◆':
            return '#0b72b4'
        case '▲':
            return '#e60a22'
        case '▼':
            return '#59b3e5'
        case _:
            return ''