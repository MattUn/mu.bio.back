from django.contrib.auth.hashers import make_password, check_password
from django.db import IntegrityError
from django.utils.crypto import get_random_string

from fyp_api_server.models import Config


def create_new():
    success = False
    config_code = ''
    access_code = ''
    while not success:#would loop in the unlikely chance of a key collision
        config_code = get_random_string(length=8)#secure random 8 char strings
        access_code = get_random_string(length=8)
        hashed_access_code = make_password(access_code)#no need to salt as already secure random
        #store config_code and hashed_access_code in db
        try:
            Config.objects.create(config_id=config_code,config_password=hashed_access_code)
            success=True
        except IntegrityError:#Will occur if unique constraint violated
            success = False
    return {'config_code': config_code, 'access_code': access_code}



def check(config_code, access_code):#verifying codes using built in password hashing and checking function
    found_config = Config.objects.filter(config_id=config_code).values('config_password')
    if found_config:
        return check_password(access_code,list(found_config)[0]['config_password'])
    else:
        return False