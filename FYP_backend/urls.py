"""FYP_backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from fyp_api_server.views import handle_start, handle_graph, handle_new, handle_load, handle_save, handle_test

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/start/<str:requested_config>',handle_start),
    path('api/graph/<str:requested_config>/<str:query_string>', handle_graph),
    path('api/make_new',handle_new),
    path('api/designer/<str:requested_config>/<str:access_code>/test',handle_test),#receive url in payload
    path('api/designer/<str:requested_config>/<str:access_code>/load',handle_load),
    path('api/designer/<str:requested_config>/<str:access_code>/save',handle_save),#receive everything in payload
]
